package traslaciontierra;

import java.awt.*;
import java.awt.image.*;
import java.io.*;
import javax.imageio.*;
import javax.media.j3d.*;
import javax.vecmath.*;

public class Fondo3D extends Background {

    public Fondo3D() {
        Bounds bounds = new BoundingSphere();
        setApplicationBounds(bounds);
        this.setCapability(Background.ALLOW_COLOR_READ);
        this.setCapability(Background.ALLOW_COLOR_WRITE);
        this.setCapability(Background.ALLOW_IMAGE_READ);
        this.setCapability(Background.ALLOW_IMAGE_WRITE);
    }
    
    public Fondo3D(Color color){
        this();
        setColor(color);
    }

    public Fondo3D(float r, float g, float b){
        this();
        setColor(r,g,b);
    }
    
    public Fondo3D(File archivo){
        this();
        setImagen(archivo);
    }
    
    public final void setColor(Color color) {
        setImage(null);
        setColor(new Color3f(color));
    }

    public final void setImagen(File archivo) {
        try {
            BufferedImage imagen = ImageIO.read(archivo);
            ImageComponent2D imagen2D = 
                    new ImageComponent2D(ImageComponent2D.FORMAT_RGB, imagen);
            setImage(imagen2D);
        } catch (IOException ex) {
            System.out.println("Error "+ex);
        }
    }
}
