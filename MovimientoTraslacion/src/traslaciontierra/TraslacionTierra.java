package traslaciontierra;

import javax.swing.JFrame;

public class TraslacionTierra{

    public static void main(String[] args) {
        PanelMovimiento panel = new PanelMovimiento();
        OyenteMovimiento oyente = new OyenteMovimiento(panel);
        panel.addEventos(oyente);
        JFrame f = new JFrame("Tierra en Movimiento");
        f.setSize(800, 600);
        f.setLocation(50, 50);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.add(panel);
        f.setVisible(true);
    } 
}
