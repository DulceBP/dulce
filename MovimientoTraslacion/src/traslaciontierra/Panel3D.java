package traslaciontierra;

import com.sun.j3d.utils.behaviors.vp.*;
import com.sun.j3d.utils.universe.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.media.j3d.*;
import javax.swing.*;

public abstract class Panel3D extends JPanel {

    private SimpleUniverse universo;
    private BranchGroup ramaContenido;
    private Fondo3D fondo;

    public Panel3D() {
        setLayout(new BorderLayout());
        addUniverso();
    }

    public final void addUniverso() {
        // Obtenemos la configuracion predeteminada de la interfaz
        GraphicsConfiguration gc
                = SimpleUniverse.getPreferredConfiguration();
        Canvas3D cv = new Canvas3D(gc);
        add(cv, "Center");
        // SimpleUniverse define los elementos basicos de una escena 3D 
        // y lo asocia al plano 2D
        universo = new SimpleUniverse(cv);
        // Define la ubicación predeterminada de la vista
        // Mueve el centro (0,0,0) a una distancia de 2.
        universo.getViewingPlatform().setNominalViewingTransform();
        creaRamaContenido();
    }

    private void creaRamaContenido() {
        ramaContenido = new BranchGroup();
        //agrega una textura de espacio al fondo del universo simple
        fondo = new Fondo3D(new File("img/es4.jpg"));
        ramaContenido.addChild(fondo);
        ramaContenido.addChild(addNodoPrincipal());
        universo.addBranchGraph(ramaContenido);
    }

    public abstract Node addNodoPrincipal();

    /**
     * @param orbitBehavior the orbitBehavior to set
     */
    public void setOrbitBehavior(boolean orbitBehavior) {
        // Se obtiene el plano 2D y la vista 3D
        Canvas3D cv = universo.getCanvas();        
        ViewingPlatform vp = universo.getViewingPlatform();
        if (orbitBehavior) {
            // Se ajusta el objeto OrbitBehavior
            OrbitBehavior orbita = 
                    new OrbitBehavior(cv, OrbitBehavior.REVERSE_ALL);
            orbita.setSchedulingBounds(new BoundingSphere());
            vp.setViewPlatformBehavior(orbita);
        } else {
            vp.setViewPlatformBehavior(null);
        }
    }

    /**
     * @return the fondo
     */
    public Fondo3D getFondo() {
        return fondo;
    }

    /**
     * @param fondo the fondo to set
     */
    public void setFondo(Fondo3D fondo) {
        this.fondo = fondo;
    }
}
