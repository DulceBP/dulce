
package traslaciontierra;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import static java.lang.Thread.sleep;
import javax.media.j3d.Transform3D;
import javax.vecmath.Matrix4f;
import javax.vecmath.Vector3f;

public class OyenteMovimiento implements ActionListener {
    // Creamos las variables protegidas 
    // para poderlas usar en otras partes si se herada la clase
    protected PanelMovimiento panel;
    protected boolean bandera;
    
    public OyenteMovimiento(PanelMovimiento panel){
     this.panel = panel;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        // aqui sabemos si le dieron click al boton
        if(!bandera){
            new HiloMovimiento().start();
            bandera = true;
        }
    }
    
 class HiloMovimiento extends Thread {
        
    @Override
    public void run() {
        //inicializamos los valores en 0 de 
        // Tralascion y Rotacion
        int anguloT = 0;
        int anguloR = 0;
        
        // la velocidad rotacion
        double r = 0.5;
        
        // Creamos los objetos de transformed
        // y utlizamos 2 matrices para darles movimiento
        Transform3D t3d = new Transform3D();
        Matrix4f matrizT = new Matrix4f();  //Matriz con ceros 
        Matrix4f matrizR = new Matrix4f();

        // cremos un ciclo infinito usando una for para el hilo
        for (;;) {
            // creamos un variable para almacenar el angulo
            // convertido en radianes
            double radT = Math.toRadians(anguloT);
            
            //damos los valores de seno y coseno para generar
            //el movimiento al ser un moviento en 2 ejes dejaremos
            // el 3ro en 0 para llenar la matriz
            float x = (float) (r * Math.cos(radT));
            float y = (float) (r * Math.sin(radT));
            float z = 0;
            /*
            MOVIMIENTO DE TRASLACION INVERTIDO
            
            float x = (float) (r * Math.cos(-radT));
            float y = (float) (r * Math.sin(-radT));
            float z = 0;
            */
            
            //usando los metodos de las matrices les pasamos los valores
            //esta matriz genera el movimiento de traslacion
            matrizT.setIdentity();
            matrizT.setM03(x);
            matrizT.setM13(y);
            matrizT.setM23(z);
 
            matrizR.setIdentity();
            // creamos un variable para almacenar el angulo
            // convertido en radianes
            double radR = Math.toRadians(anguloR);
            
            //damos los valores de seno y coseno para generar
            //el movimiento al ser un moviento en 3 ejes dejaremos
            float seno = (float) Math.sin(radR);
            float coseno = (float) Math.cos(radR);
            
            /*
            MOVIMIENTO INVERTIDO DEL PLANETA SOBRE SU EJE
            
            float seno = (float) Math.sin(-radR);
            float coseno = (float) Math.cos(-radR);
            */
            
            //usando los metodos de las matrices les pasamos los valores
            matrizR.setM00(coseno);
            matrizR.setM22(coseno);
            matrizR.setM02(seno);
            matrizR.setM20(-seno);

            matrizT.mul(matrizR);
            t3d.set(matrizT);
            panel.getTg().setTransform(t3d);

            try {
                sleep(50);

            } catch (InterruptedException e) {
                System.out.println("Error " + e);
                System.exit(0);
            }
            anguloT -= 1;
            anguloR += 2;
        }
    }
}
    
}

