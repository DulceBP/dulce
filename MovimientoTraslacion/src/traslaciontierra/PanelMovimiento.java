package traslaciontierra;

import com.sun.j3d.utils.geometry.*;
import com.sun.j3d.utils.image.TextureLoader;
import java.awt.Container;
import javax.media.j3d.*;
import javax.swing.*;

public class PanelMovimiento extends Panel3D {
    // Creamos las variables privadas
    // para poderlas usar solamente en este parte del codigo
    private JButton botonIniciar;
    private TransformGroup tg;

    public PanelMovimiento(){
        // indicamos el comportamiento
        this.setOrbitBehavior(true);
        addComponentes();
    }
    
    private void addComponentes() {
        // agregamos los componentes del panel
        // un boton el cual no permitira iniciar el hilo
        JPanel panelNorte = new JPanel();
        botonIniciar = new JButton("Iniciar Movimiento");
        panelNorte.add(botonIniciar);
        // lo añadimos al panel norte ("North")
        add(panelNorte,"North");        
    }
    
    public void addEventos(OyenteMovimiento oyente){
        // agregar el oyente para cuando sepamos
        // en que momento se activa
        botonIniciar.addActionListener(oyente);
    }

    @Override
    public Node addNodoPrincipal() {
        
        // Creamos una variable para el radio de la esfera
        float radio =0.2f;
        
        // Creamos un objeto Sphere
        Sphere s;
        
        // Creamos una variable para poder añadir la textura
        // a la esfera
        int paratextura=Primitive.GENERATE_NORMALS+Primitive.GENERATE_TEXTURE_COORDS;
        
        // inicializamos la sphere y la creamos y
        // llamamos al metodo crearApariencia
        s = new Sphere(radio, paratextura, crearApariencia());
        
        //creamos un TransformGroup para añadir la esfera
        tg = new TransformGroup();
        tg.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        // Añadimos la esfera a nuestro transformgroup
        tg.addChild(s);
        
        //creamos un BranchGroup para añadir el transformgroup
        // y las iluminaciones
        BranchGroup scene = new BranchGroup();
        
        // creamos una luz direccional
        // y le indicamos que tengo un efecto BoundingSphere
        DirectionalLight lightD1 = new DirectionalLight();
        lightD1.setInfluencingBounds(new BoundingSphere());
        // añadimos la luz a nuestro branchgroup
        scene.addChild(lightD1);
        
        // creamos una luz ambiental
        // y le indicamos que tengo un efecto BoundingSphere
        AmbientLight lightA = new AmbientLight();
        lightA.setInfluencingBounds(new BoundingSphere());
        // añadimos la luz a nuestro branchgroup
        scene.addChild(lightA);
        
        // añadimos nuestra esfera que teniamos en el
        // transformgroup a nuestro branchgroup
        scene.addChild(tg);
        
        //regresamos el branchgroup que contiene las luces
        // y la esfera
        return scene;
    }
    
    // Metodo para crear la apariencia
    public Appearance crearApariencia(){
        // metodo para crear la textura de la tierra
        TextureLoader loader = new TextureLoader("img/jupiter.jpg",new Container());
        Texture texture = loader.getTexture();
        Appearance apariencia=new Appearance();
        apariencia.setTexture(texture);
        Material material = new Material();
        apariencia.setMaterial(material);
 
        return apariencia;
    }
    /**
     * @return the tg
     */
    public TransformGroup getTg() {
        return tg;
    }

    /**
     * @param tg the tg to set
     */
    public void setTg(TransformGroup tg) {
        this.tg = tg;
    }


}
